define([
  'underscore',
  'backbone',
  'models/userModel',
], function(_, Backbone, userModel) {

  var userCollection = Backbone.Collection.extend({
    url: "http://localhost/wp_test/wp-json/wpchat/v1/users/",
    model: userModel,

    fetch: function(options) {
      self = this;
      fetch("http://localhost/wp_test/wp-json/wpchat/v1/users/", {
          method: "GET",
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('wptoken'),
          },
        }).then(function(response){
          return response.json();
        }).then(function(post){
            data = JSON.parse(post);
            self.reset(data);
      })
    },

    initialize: function () {
      this.fetch();
    },

    parse: function(response) {
      response = JSON.parse(response);
      return response;
    }
  });

  return new userCollection;

});