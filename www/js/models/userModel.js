define([
  'underscore',
  'backbone',
  'controlPanel'
], function(_, Backbone, controlPanel) {

  var userModel = Backbone.Model.extend({
    initialize: function() {
      this.startMessageListening();
    },

    newMessage: '',

    startMessageListening: function() {
      var thats = this;
      this.interval = setInterval(function() {
        thats.checkNewMessages();
      }, 10000);
      window.intervals.push(this.interval);
    },

    checkNewMessages: function() {
      var self = this;

      fetch('http://localhost/wp_test/wp-json/wpchat/v1/last-message-seen/', {
        method: "POST",
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer ' + localStorage.getItem('wptoken'),
        },
          body: JSON.stringify({
              user: self.get('ID')
          })
      }).then(function(response){
        return response.json();
      }).then(function(post){
        if (post=='false') {
          self.set({'newMessage': 'true'});
          //self.markNewMessage();
        } else if (post=='true') {
          self.set({'newMessage': 'false'});
          //self.removeMarkNewMessage();
        }
        console.log(self);
      });
    },
  });

  return userModel;

});