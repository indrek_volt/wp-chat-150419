define([
  'underscore',
  'backbone',
], function(_, Backbone) {

  var headerModel = Backbone.Model.extend({
    url: 'http://localhost/wp_test/wp-json/wpchat/v1/getinfo/',
    name: ''
  });

  return headerModel;

});