define([
  'jquery',
  'underscore',
  'backbone',
  'views/login/loginView',
  'views/containerView',
  'views/main/mainView'
], function($, _, Backbone, loginView, containerView, mainView, headerView) {
  
  var AppRouter = Backbone.Router.extend({
    container: '',
    routes: {
      // Define URL routes
      'login': 'loginView',
      'main':  'mainView',
      // Default
      '*actions': 'loginView'
    }
  });
  
  var initialize = function() {
    var app_router = new AppRouter;

    window.intervals = [];

    that = this;

    app_router.on('route:loginView', function() {
      if ( that.container == null ) {
        that.container = new containerView();
      }

      if (this.loginScreen != null) {
        this.loginScreen.destroyView();
      }

      this.loginScreen = new loginView();

      that.container.myChildView = this.loginScreen;
      that.container.render();
    });

    app_router.on('route:mainView', function() {
      if ( that.container == null ) {
        that.container = new containerView();
      }

      if (this.mainScreen != null) {
        this.mainScreen.destroyView();
      }

      this.mainScreen = new mainView();

      that.container.myChildView = this.mainScreen;
      that.container.render();
    });

    Backbone.history.start();
  };

  return {
    initialize: initialize
  };
});
