define([
  'jquery',
  'underscore',
  'backbone',
  'text!templates/login/loginTemplate.html',
  'controlPanel',
  'views/baseView'
], function($, _, Backbone, loginTemplate, controlPanel, baseView, ){

  var loginView = baseView.extend({
    events: {
     'click #login-button'   : 'login',
    },

    template: loginTemplate,
    that: this,

    login: function() {
      var loginName = this.$('#login-name').val();
      var loginPassword = this.$('#login-password').val();
      that = this;
      fetch('http://localhost/wp_test/wp-json/jwt-auth/v1/token',{
        method: "POST",
        headers:{
          'Content-Type': 'application/json',
          'accept': 'application/json',
        },
        body:JSON.stringify({
          username: loginName,
          password: loginPassword,
          view: 'teacher'
        })
      }).then(function(response){
          return response.json();
      }).then(function(user){
          if (user.token) {
            controlPanel.trigger("loginSuccess");
            localStorage.setItem('wptoken', user.token);
            window.location.href = 'index.html#/main';
          } else {
            that.showElement(this.$('#login-fail'));
          }
      });
    },
  });

  return loginView;
});