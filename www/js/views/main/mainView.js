define([
  'jquery',
  'underscore',
  'backbone',
  'text!templates/main/mainTemplate.html',
  'views/baseView',
  'views/header/headerView',
  'views/sidebar/sidebarView',
  'views/chat/chatView'
], function($, _, Backbone, mainTemplate, baseView, headerView, sidebarView, chatView){

  var mainView = baseView.extend({
    template: mainTemplate,
    header: '',

    initialize: function() {
      this.render();
    },

    render: function() {

      var that = this;

      var compiledTemplate = new Promise(
          function (resolve, reject) {
              var compiledTemplate = _.template( that.template );
              that.$el.html(compiledTemplate);

              resolve();
          }
      );

      function createComponents() {
          return new Promise(
              function (resolve, reject) {
                  var now = new Date();
                  var startOfDay = new Date(now.getFullYear(), now.getMonth(), now.getDate());
                  var timestamp = startOfDay / 1000;

                  that['header'+timestamp] = new headerView({el: "#main-header"});
                  that['sidebar'+timestamp] = new sidebarView({el: "#sidebar"});
                  that['mainChat'+timestamp] = new chatView({el: "#main-chat-container"});
              }
          );
      }

      compiledTemplate.then(createComponents);

      return this;
    }
  });

  return mainView;
});
