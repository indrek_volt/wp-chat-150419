define([
  'jquery',
  'underscore',
  'backbone',
  'text!templates/user/userTemplate.html',
  'views/baseView',
  'models/chatModel',
  'controlPanel',
], function($, _, Backbone, userTemplate, baseView, chatModel, controlPanel){

  var userView = Backbone.View.extend({
    initialize: function() {
      var that10 = this;
      this.model.on('change:newMessage', function() {
        if (that10.model.get('newMessage')=='true') {
          that10.$el.find('img').addClass('active');
        } else {
          that10.$el.find('img').removeClass('active');
        }
      });
      setTimeout(function() {
        console.log(55555555);
        console.log(that10.model);
      }, 4000);

      this.render();
    },

    events: {
      "click":          "selectUser",
    },

    template: userTemplate,

    selectUser: function() {
      chatModel.set({userID: this.model.get('ID')});
      chatModel.set({userDisplayName: this.model.get('display_name')});
      controlPanel.trigger("convSelected");
      $('.sidebar-user').removeClass('sidebar-user--active'); //TODO: Should be done in sidebar view
      this.$el.find('.sidebar-user').addClass('sidebar-user--active');
    },

    render: function() {
      console.log(this.model);
      var compiledTemplate = _.template( this.template, this.model.toJSON() );
      this.$el.html(compiledTemplate);
      return this;
    }
  });

  return userView;
});

