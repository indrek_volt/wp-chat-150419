define([
  'jquery',
  'underscore',
  'backbone',
  'models/chatModel',
  'text!templates/chat/chatTemplate.html',
  'collections/messageCollection',
  'controlPanel'
], function($, _, Backbone, chatModel, chatTemplate, messageCollection, controlPanel){

  var chatView = Backbone.View.extend({
    activeChatInterval: '',

    events: {
      'click #main-submit': 'chatMessage',
    },

    initialize: function() {
      messageCollection.on("reset", this.render);
    },

    chatMessage: function() {

      var message = this.$('input[name="main-text"]').val();

      var that = this;

      fetch('http://localhost/wp_test/wp-json/wpchat/v1/addmessage/', {
        method: "POST",
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer ' + localStorage.getItem('wptoken'),
        },
          body: JSON.stringify({
              content: [message, chatModel.get('userID')]
          })
      }).then(function(response){
        return response.json();
      }).then(function(post){
        controlPanel.trigger("newPostAdded");
        that.$('input[name="main-text"]').val('');
      });
    },

    render: function() {
      if (messageCollection.length != 0) {
        var compiledTemplate = _.template( chatTemplate,{messages:messageCollection.toJSON()} );
        $('#main-chat-inner').html(compiledTemplate);
        var objDiv = document.getElementById("main-chat");
        if($("#main-chat").length){
          objDiv.scrollTop = objDiv.scrollHeight;
        }
      } else {
        $('#main-chat-inner').empty();
      }

      return this;
    }
  });

  return chatView;
});
