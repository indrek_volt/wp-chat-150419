define([
  'jquery',
  'underscore',
  'backbone',
  'views/baseView',
  'collections/userCollection',
  'views/user/userView',
  'controlPanel'
], function($, _, Backbone, baseViev, userCollection, userView){

  var sidebarView = Backbone.View.extend({

    initialize: function() {
      currentContext = this;
      userCollection.on("reset", this.render);
      userCollection.fetch();
    },

    userViews: [],

    render: function() {
      if (userCollection.length != 0) {
        currentContext.$el.empty();
        userCollection.each(function(person) {
          var userScreen = new userView({model: person});
          currentContext.$el.append(userScreen.el);
        }, currentContext);
      }
    }
  });

  return sidebarView;
});
