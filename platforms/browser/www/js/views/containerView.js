define([
  'jquery',
  'underscore',
  'backbone',
], function($, _, Backbone){

  var containerView = Backbone.View.extend({
    el: '#maincont',
    myChildView: null,

    render: function(){
      this.$el.html(this.myChildView.el);
      return this;
    }

  });

  return containerView;
});
