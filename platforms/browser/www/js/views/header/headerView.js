define([
  'jquery',
  'underscore',
  'backbone',
  'text!templates/header/headerTemplate.html',
  'views/baseView',
  'models/headerModel',
  'collections/userCollection',
  'collections/messageCollection',
  'models/chatModel',
  'controlPanel'
], function($, _, Backbone, headerTemplate, baseView, headerModel, userCollection, messageCollection, chatModel, controlPanel){

  var headerView = Backbone.View.extend({

    initialize: function() {
      this.getInfo();
    },

    model: new headerModel(),

    getInfo: function() {
      var that = this;
      fetch('http://localhost/wp_test/wp-json/wpchat/v1/getinfo/', {
        method: "POST",
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer ' + localStorage.getItem('wptoken'),
        }
      }).then(function(response){
        return response.json();
      }).then(function(post){
        var user = JSON.parse(post);
        var userarray = user[0];
        that.model.set({'name': userarray['data']['display_name']});
        that.render();
      });
    },


    template: headerTemplate,

    events: {
      'click #header-logout': 'logOut',
    },

    logOut: function() {
      $.each(window.intervals, function(index, value) {
          window.clearInterval(value);
      });

      controlPanel.trigger("loggedOut");
      userCollection.reset();
      messageCollection.reset();
      chatModel.set({userID: ''});
      chatModel.set({userDisplayName: ''});
      $('#main-chat-inner').empty();
      localStorage.setItem('wptoken', undefined);
      window.location.href = 'index.html#/login';
    },

    render: function() {
      var compiledTemplate = _.template( this.template, this.model.toJSON() );
      this.$el.html(compiledTemplate);
      return this;
    }
  });

  return headerView;
});

