define([
  'jquery',
  'underscore',
  'backbone',
], function($, _, Backbone){

  var baseView = Backbone.View.extend({
    initialize: function() {
      this.render();
    },

    showElement: function(element) {
      element.removeClass('display-none');
    },

    hideElement: function(element) {
      element.addClass('display-none');
    },

    destroyView: function() {
      // Completely unbind the view.
      this.undelegateEvents();

      this.$el.removeData().unbind();

      // Remove view from DOM.
      this.remove();
      Backbone.View.prototype.remove.call(this);
    },

    render: function(){
      var compiledTemplate = _.template( this.template );
      this.$el.html(compiledTemplate);
      return this;
    }
  });

  return baseView;
});
