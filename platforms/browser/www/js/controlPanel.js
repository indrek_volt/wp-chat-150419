define([
  'jquery',
  'underscore',
  'backbone',
], function($,_, Backbone){

  var controlPanel = {};

  _.extend(controlPanel, Backbone.Events);

  return controlPanel;
});
