define([
  'underscore',
  'backbone',
  'models/messageModel'
], function(_, Backbone, messageModel) {

  var messageCollection = Backbone.Collection.extend({
    url: "http://localhost/wp_test/wp-json/wpchat/v1/messages/",
    model: messageModel,

    arraysEqual: function(_arr1, _arr2) {

        if (!Array.isArray(_arr1) || ! Array.isArray(_arr2) || _arr1.length !== _arr2.length)
          return false;

        var arr1 = _arr1.concat().sort();
        var arr2 = _arr2.concat().sort();

        for (var i = 0; i < arr1.length; i++) {
            if (JSON.stringify(arr1[i]) !== JSON.stringify(arr2[i])) {
              return false;
            }

        }

        return true;

    },

    fetch: function(options) {
      self = this;
      fetch("http://localhost/wp_test/wp-json/wpchat/v1/messages/", {
          method: "POST",
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('wptoken'),
          },
            body: JSON.stringify({
                talkable: options.talkable
            })
        }).then(function(response){
          return response.json();
        }).then(function(post){
          data = JSON.parse(post);

          selfModels = [];
          $.each(self.models, function( index, value ) {
            selfModels.push(value.attributes);
          });

          if (!self.arraysEqual(data, selfModels)) {
            self.reset(data);
          } else {
          }

      })
    },

    parse: function(response) {
      response = JSON.parse(response);
      return response;
    }
  });

  return new messageCollection;

});