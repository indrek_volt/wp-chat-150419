define([
  'underscore',
  'backbone',
  'collections/messageCollection',
  'controlPanel'
], function(_, Backbone, messageCollection, controlPanel) {

  var chatModel = Backbone.Model.extend({
    initialize: function() {
      this.on("change:userID", this.getMessages, this);
      that0 = this;
      controlPanel.on("convSelected", this.startMessageListening);
      controlPanel.on('newPostAdded', (function(){
          return function(){
              messageCollection.fetch({talkable: that0.get('userID')});
          }
      })());
    },

    getMessages: function() {
      messageCollection.fetch({talkable: this.get('userID')});
    },

    startMessageListening: function() {
      clearInterval(this.mesageInterval);
      this.mesageInterval = setInterval(function() {
        that0.getMessages();
      }, 3000);
      window.intervals.push(this.mesageInterval);
    },

    user: '',
  });

  return new chatModel;

});